<?php namespace CodeCommerce\Http\Requests;

use CodeCommerce\Http\Requests\Request;

class ProductRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		// default the value checkbox
		$this->merge(['featured' => $this->input('featured', false)]);
		$this->merge(['recommend' => $this->input('recommend', false)]);

		return [
			'name' => 'required|min:3',
			'description' => 'required|min:3',
			'price' => 'required',
			'tags' => 'required'
		];
	}

}
