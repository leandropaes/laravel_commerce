<?php namespace CodeCommerce;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['category_id', 'name', 'description', 'price', 'featured', 'recommend'];

    public function category()
    {
        return $this->belongsTo('CodeCommerce\Category');
    }

    public function images()
    {
        return $this->hasMany('CodeCommerce\ProductImage');
    }

    public function tags()
    {
        return $this->belongsToMany('CodeCommerce\Tag');
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', '=', 1);
    }

    public function scopeRecommend($query)
    {
        return $query->where('recommend', '=', 1);
    }

    public function scopeByCategory($query, $category_id)
    {
        return $query->where('category_id', '=', $category_id);
    }

    public function getNameDescriptionAttribute()
    {
        return $this->name . ' - ' . $this->description;
    }

    public function getTagListAttribute()
    {
        $tags = $this->tags->lists('name');

        return implode(',', $tags);
    }

    public function setTags($tag_list)
    {
        $tas_add = [];
        $tags = explode(',', $tag_list);
        foreach($tags as $tagName) {
            $tag = Tag::firstOrCreate(['name' => trim($tagName)]);
            $tas_add[] = $tag->id;
        }
        $this->tags()->sync($tas_add);
    }
}
