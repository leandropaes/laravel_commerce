@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Category: {{ $category->name }}</h2>

                @include('partials.form-errors')

                {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT']) !!}

                @include('categories.partials.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop