@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Product</h2>

                @include('partials.form-errors')

                {!! Form::open(['route' => 'products.store']) !!}

                @include('products.partials.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop