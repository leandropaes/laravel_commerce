@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>
                    Images of {{ $product->name }}
                    <a href="{{ route('products.images.create', [$product->id]) }}" class="btn btn-primary pull-right" style="margin-left: 10px;ç">New Image</a>
                    <a href="{{ route('products') }}" class="btn btn-default pull-right">Back</a>
                </h2>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Extension</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product->images as $image)
                        <tr>
                            <td>{{ $image->id }}</td>
                            <td><img src="{{ url('uploads/' . $image->id.'.'.$image->extension) }}" alt="" height="30"/></td>
                            <td>{{ $image->extension }}</td>
                            <td>
                                <a href="{{ route('products.images.destroy', [$image->id]) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@stop